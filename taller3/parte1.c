#include <stdio.h>
#include <string.h>
#define TAMANO_MAX 100
#define MAX 100
void reemplazar(char linea[],char viejo,char nuevo);
int main()
{
	int num, aux;
	float suma=0;
	int numeros[TAMANO_MAX] = {0};
	printf("Cuántos números va a ingresar?: ");
	scanf("%d", &num);
	getchar();
	while(num > TAMANO_MAX || num < 1){
		printf("Error, número máximo es 100...\n");
		printf("Ingrese de nuevo un número: ");
		scanf("%d", &num);
		getchar();
	}
	char linea[MAX]={0};

	for(int i=0;i<num;i++){
		printf("Ingrese número %d : ", i+1);
		fgets(linea,MAX,stdin);
		reemplazar(linea, '\n' , '\0');
		sscanf(linea, "%d" , &(numeros[i]));
	}

	for(int i=0; i<num;i++){
		aux=numeros[i];
		suma+=aux;
	}
	printf("El promedio es: %.2f\n", suma/num);
	return 0;
}

void reemplazar(char linea[],char viejo,char nuevo){
	size_t n= strlen(linea);
	for(int i=0;i<n;i++) {
		char c = linea[i];
		if (c == viejo) {
			linea[i] = nuevo;
		}
 	}
}

